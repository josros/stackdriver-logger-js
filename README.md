# Stackdriver logger

Enables you to quickly log to google cloud engine stackdriver from your web application frontend.

## How to prepare

1. Create a gcp iam service account for your project [https://console.cloud.google.com/iam-admin/serviceaccounts](https://console.cloud.google.com/iam-admin/serviceaccounts)
   1. Select an arbitrary service account name
   1. Select the role `Logs Writer` (this allows the sevice account to issue a jwt with the scope `https://www.googleapis.com/auth/logging.write`)
   1. Create and download a private key file (from the file content you will need `private_key` and `client_email`)
1. Install this library as dependency:

   ```bash
   npm install git+https://gitlab.com/josros/stackdriver-logger-js.git#0.1.0 --save
   ```

## How to use

### Typescript example

```ts
import { StackdriverLogger } from '@josros/stackdriver-logger';

// instantiate the logger somewhere in your application
// this.issuer is the `client_email` from the private key file
// whereas this.privateKey is the `private_key` from there
const stackdriver = new StackdriverLogger(
  this.issuer,
  this.privateKey,
  'your-app-name'
);

// this is just an example, you can log whatever entries you want
// see: https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
const logValue = [
  {
    jsonPayload: {
      application: 'my-app',
      text: 'Hello from my-app',
    },
  },
  {
    jsonPayload: {
      application: 'my-app',
      text: 'Another hello from my-app',
    },
  },
];

// await for the log to be written
await this.stackdriver.log(logValue);
```

## Current state

The current implementation solves the problem of directly writing logs to stackdriver from your web applicaiton frontend project. It handles authentication and token renewal, but it does not implement log buffering. You can use this library in a non blocking way, but to keep your application respnsive you should buffer your logs and write them in batch mode.
Note that I've implemented basic functionality to write logs to stackdriver. I have reduced the complexity of the stackdriver api to what is really needed to get started.
If you need more of the stackdriver api to be supported, please feel free to contribute to this library. Pull requests are welcome.
