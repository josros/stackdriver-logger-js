/**
 * Implements the token retrieval described in: https://developers.google.com/identity/protocols/OAuth2ServiceAccount
 */
export declare class JwtGeneration {
    private static readonly SIGNATURE_ALGORITHM;
    createJwt(issuer: string, privateKey: string): Promise<string>;
    private signToken;
}
