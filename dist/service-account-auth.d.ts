export declare class ServiceAccountAuth {
    private static readonly TOKEN_URL;
    retrieveAccessToken(jwt: string): Promise<Token>;
    private postData;
}
export interface Token {
    access_token: string;
    expires_at_seconds: number;
}
