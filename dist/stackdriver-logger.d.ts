export declare class StackdriverLogger {
    private static readonly LOG_WRITE_URL;
    private static readonly EXPIRATION_BUFFER_SECONDS;
    private readonly serviceAccountEmail;
    private readonly serviceAccountPrivateKey;
    private readonly projectId;
    private jwtGeneration;
    private serviceAccountAuth;
    private cachedToken;
    constructor(serviceAccountEmail: string, serviceAccountPrivateKey: string, projectId: string);
    /**
     *
     * @param logentries are the entries to be logged as json array. It follows the specification in
     * https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
     *
     * "entries": [
     *  {
     *    object (LogEntry)
     *  }
     * ],
     *
     * for a single log it could look as follows:
     *
     * [
     *  {
     *    jsonPayload: {
     *      key: "value"
     *    }
     *  }
     * ],
     *
     */
    log(logentries: {}): Promise<void>;
    private accessToken;
    private newToken;
    private expiresSoon;
    private createLogJson;
    private postData;
}
