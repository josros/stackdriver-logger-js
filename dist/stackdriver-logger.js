"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var jwt_gen_1 = require("./jwt-gen");
var service_account_auth_1 = require("./service-account-auth");
var StackdriverLogger = /** @class */ (function () {
    function StackdriverLogger(serviceAccountEmail, serviceAccountPrivateKey, projectId) {
        this.jwtGeneration = new jwt_gen_1.JwtGeneration();
        this.serviceAccountAuth = new service_account_auth_1.ServiceAccountAuth();
        this.cachedToken = null;
        this.serviceAccountEmail = serviceAccountEmail;
        this.serviceAccountPrivateKey = serviceAccountPrivateKey;
        this.projectId = projectId;
    }
    /**
     *
     * @param logentries are the entries to be logged as json array. It follows the specification in
     * https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
     *
     * "entries": [
     *  {
     *    object (LogEntry)
     *  }
     * ],
     *
     * for a single log it could look as follows:
     *
     * [
     *  {
     *    jsonPayload: {
     *      key: "value"
     *    }
     *  }
     * ],
     *
     */
    StackdriverLogger.prototype.log = function (logentries) {
        return __awaiter(this, void 0, void 0, function () {
            var body, accessToken;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        body = this.createLogJson(logentries);
                        return [4 /*yield*/, this.accessToken()];
                    case 1:
                        accessToken = _a.sent();
                        return [4 /*yield*/, this.postData(StackdriverLogger.LOG_WRITE_URL, accessToken, body)];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    StackdriverLogger.prototype.accessToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        if (!(this.cachedToken && !this.expiresSoon(this.cachedToken))) return [3 /*break*/, 1];
                        return [2 /*return*/, this.cachedToken.access_token];
                    case 1:
                        _a = this;
                        return [4 /*yield*/, this.newToken()];
                    case 2:
                        _a.cachedToken = _b.sent();
                        return [2 /*return*/, this.cachedToken.access_token];
                }
            });
        });
    };
    StackdriverLogger.prototype.newToken = function () {
        return __awaiter(this, void 0, void 0, function () {
            var jwt;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.jwtGeneration.createJwt(this.serviceAccountEmail, this.serviceAccountPrivateKey)];
                    case 1:
                        jwt = _a.sent();
                        return [2 /*return*/, this.serviceAccountAuth.retrieveAccessToken(jwt)];
                }
            });
        });
    };
    StackdriverLogger.prototype.expiresSoon = function (token) {
        var nowSeconds = Date.now() / 1000;
        return (token.expires_at_seconds <
            nowSeconds + StackdriverLogger.EXPIRATION_BUFFER_SECONDS);
    };
    // see: https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
    StackdriverLogger.prototype.createLogJson = function (entries) {
        return {
            logName: "projects/" + this.projectId + "/logs/default",
            entries: entries,
            resource: {
                type: 'global',
                labels: {
                    project_id: "" + this.projectId,
                },
            },
        };
    };
    StackdriverLogger.prototype.postData = function (url, accessToken, data) {
        if (data === void 0) { data = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, fetch(url, {
                            method: 'POST',
                            cache: 'no-cache',
                            headers: {
                                Authorization: "Bearer " + accessToken,
                                'Content-Type': 'application/json',
                                Accept: 'application/json',
                            },
                            body: JSON.stringify(data),
                        })];
                    case 1:
                        response = _a.sent();
                        return [2 /*return*/, response];
                }
            });
        });
    };
    StackdriverLogger.LOG_WRITE_URL = 'https://logging.googleapis.com/v2/entries:write';
    StackdriverLogger.EXPIRATION_BUFFER_SECONDS = 10;
    return StackdriverLogger;
}());
exports.StackdriverLogger = StackdriverLogger;
//# sourceMappingURL=stackdriver-logger.js.map