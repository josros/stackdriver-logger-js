import jwt from 'jsonwebtoken';
/**
 * Implements the jwt generation used during token retrieval described in: https://developers.google.com/identity/protocols/OAuth2ServiceAccount
 */
export class JwtGeneration {
  private static readonly SIGNATURE_ALGORITHM = 'RS256';

  /**
   * Creates a json web token required for authentication when requesting an access token later used to write logs to stackdriver.
   *
   * @param issuer the `client_email` of your logs writer gce service account
   * @param privateKey the `private_key` of your logs writer gce service account
   */
  async createJwt(issuer: string, privateKey: string): Promise<string> {
    return this.signToken(issuer, privateKey);
  }

  private async signToken(issuer: string, privateKey: string): Promise<string> {
    return new Promise((res, rej) => {
      jwt.sign(
        {
          iss: issuer,
          scope: 'https://www.googleapis.com/auth/logging.write',
          aud: 'https://oauth2.googleapis.com/token',
        },
        privateKey,
        { algorithm: JwtGeneration.SIGNATURE_ALGORITHM, expiresIn: '1h' },
        (err: Error, token: string) => {
          if (err) {
            rej(err);
          }
          res(token);
        }
      );
    });
  }
}
