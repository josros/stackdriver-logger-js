/**
 * Implements the token retrieval described in: https://developers.google.com/identity/protocols/OAuth2ServiceAccount
 */
export class ServiceAccountAuth {
  private static readonly TOKEN_URL = 'https://oauth2.googleapis.com/token';

  /**
   * Retrives an access token required to send logs to stackdriver.
   *
   * @param jwt json web token used for authentication while requesting an access token
   */
  async retrieveAccessToken(jwt: string): Promise<Token> {
    const body = `grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=${jwt}`;
    const response = await this.postData(ServiceAccountAuth.TOKEN_URL, body);
    if (response.ok) {
      const data = await response.json();
      if (data.access_token && data.expires_in) {
        const expiresAt = Date.now() + data.expires_in * 1000;
        const expiresAtSeconds = expiresAt / 1000;
        return {
          access_token: data.access_token,
          expires_at_seconds: expiresAtSeconds,
        };
      }
    }
    throw new Error('Error retrieving access token');
  }

  private async postData(url: string, body: string): Promise<Response> {
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body,
    });
    return response;
  }
}

export interface Token {
  access_token: string;
  expires_at_seconds: number;
}
