import { JwtGeneration } from './jwt-gen';
import { ServiceAccountAuth, Token } from './service-account-auth';

/**
 * Writes logs to the stackdriver api. Here you can find the api documentation:
 * https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
 */
export class StackdriverLogger {
  private static readonly LOG_WRITE_URL =
    'https://logging.googleapis.com/v2/entries:write';
  private static readonly EXPIRATION_BUFFER_SECONDS = 10;

  private readonly serviceAccountEmail: string;
  private readonly serviceAccountPrivateKey: string;
  private readonly projectId: string;

  private jwtGeneration: JwtGeneration = new JwtGeneration();
  private serviceAccountAuth: ServiceAccountAuth = new ServiceAccountAuth();

  private cachedToken: Token | null = null;

  constructor(
    serviceAccountEmail: string,
    serviceAccountPrivateKey: string,
    projectId: string
  ) {
    this.serviceAccountEmail = serviceAccountEmail;
    this.serviceAccountPrivateKey = serviceAccountPrivateKey;
    this.projectId = projectId;
  }

  /**
   *
   * @param logentries are the entries to be logged as json array. It follows the specification in
   * https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
   *
   * "entries": [
   *  {
   *    object (LogEntry)
   *  }
   * ],
   *
   * for a single log it could look as follows:
   *
   * [
   *  {
   *    jsonPayload: {
   *      key: "value"
   *    }
   *  }
   * ],
   *
   */
  async log(logentries: {}) {
    const body = this.createLogJson(logentries);
    const accessToken = await this.accessToken();
    await this.postData(StackdriverLogger.LOG_WRITE_URL, accessToken, body);
  }

  private async accessToken(): Promise<string> {
    if (this.cachedToken && !this.expiresSoon(this.cachedToken)) {
      return this.cachedToken.access_token;
    } else {
      this.cachedToken = await this.newToken();
      return this.cachedToken.access_token;
    }
  }

  private async newToken(): Promise<Token> {
    const jwt = await this.jwtGeneration.createJwt(
      this.serviceAccountEmail,
      this.serviceAccountPrivateKey
    );
    return this.serviceAccountAuth.retrieveAccessToken(jwt);
  }

  private expiresSoon(token: Token): boolean {
    const nowSeconds = Date.now() / 1000;
    return (
      token.expires_at_seconds <
      nowSeconds + StackdriverLogger.EXPIRATION_BUFFER_SECONDS
    );
  }

  // see: https://cloud.google.com/logging/docs/reference/v2/rest/v2/entries/write
  private createLogJson(entries: {}): {} {
    return {
      logName: `projects/${this.projectId}/logs/default`,
      entries,
      resource: {
        type: 'global',
        labels: {
          project_id: `${this.projectId}`,
        },
      },
    };
  }

  private async postData(
    url: string,
    accessToken: string,
    data = {}
  ): Promise<Response> {
    const response = await fetch(url, {
      method: 'POST',
      cache: 'no-cache',
      headers: {
        Authorization: `Bearer ${accessToken}`,
        'Content-Type': 'application/json',
        Accept: 'application/json',
      },
      body: JSON.stringify(data),
    });
    return response;
  }
}
