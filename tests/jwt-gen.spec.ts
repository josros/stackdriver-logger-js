import { JwtGeneration } from '../src/jwt-gen';
import jwt from 'jsonwebtoken';

describe('JwtGeneration', (): void => {
  beforeEach(() => {});
  it('createJwt', async (): Promise<void> => {
    // GIVEN
    const jwtGen = new JwtGeneration();
    const mockIssuer = 'my-mock-issuer';
    // random rsa key
    const mockPk =
      '-----BEGIN RSA PRIVATE KEY-----\n' +
      'MIICXgIBAAKBgQDXKX2/PDS9KgCUZXJJVpgZ2btHNjB4MY+SU9fT5xujHxe+ET/u\n' +
      'E0rQS2xRRbAI2ntFJI4Iw/i/DfrAGlkBPoCq/OwbjcLyajNErVkTYmXAQh84WjpC\n' +
      'x8lSf8wBMlut2KSTU4Sd5NqnweiG/+Gk/EAFRTOTCsJeQK1sDJ+frB/nvQIDAQAB\n' +
      'AoGBAIRjtkjl/blrT8H/jcvMaXh2fYJ6uGYscJ/j0QhEyr+bERvVN1YBLuZtki+L\n' +
      'xp4b+P95V5/THYpJSsHLNHrMf5QVNPCghZYXKt/tR3zaWf8qgfp67xNs7zea2TTL\n' +
      '2cM1ibfUdL4uw7gPkGrVAsMjQd28Ldl8gAv0ssCm5Sq1j7whAkEA9mByEEkJRyRh\n' +
      'dNAOr7aBNO6PvjRT28h/l5Y+sWZBK6A4zs2FkG2UtHdL5GIoAhkAdZXh/C1iGc25\n' +
      'z+yfB0sIVQJBAN+Q7QwN0zEogLsBW69fI38FzO+IeTp9sZ80UilpEsLjRQclbS+W\n' +
      'OdotAH1fyIKxtncB5A7VIGJHH1mAzt4d6ckCQQCJwA7GMC+xyRN8Hj+32pCPKbrQ\n' +
      'm60vOwCrnmNbCFLzs1dPuPUudTYpWHHkNUUtNxFcrPp25mDtEGsNZ7zadQcpAkEA\n' +
      'h5lZjcQc4mYUnlhWieoEl5inEmoOTYHLaCrN4rwOWxKJ3am/retKL6C3+VNNRKgR\n' +
      'aeFFuBFg0PsecqUD1AkEsQJAXTYZQkxXQI72SJEuEd0ldu9C6jOIC7UMywHi7LbV\n' +
      '7vljm0pM1rwaBDSrY+Qw96a+nUotSPVpIjf4q4Dr010myw==\n' +
      '-----END RSA PRIVATE KEY-----';
    const mockSign = jest.spyOn(jwt, 'sign');

    // WHEN
    const jwtGenerated = await jwtGen.createJwt(mockIssuer, mockPk);

    // THEN
    expect(jwtGenerated).toContain('eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.');

    expect(mockSign).toHaveBeenCalledWith(
      {
        iss: mockIssuer,
        scope: 'https://www.googleapis.com/auth/logging.write',
        aud: 'https://oauth2.googleapis.com/token',
      },
      mockPk,
      { algorithm: 'RS256', expiresIn: '1h' },
      expect.anything()
    );
  });
});
