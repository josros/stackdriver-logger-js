import { ServiceAccountAuth } from '../src/service-account-auth';

describe('ServiceAccountAuth', (): void => {
  const fetchAny = fetch as any;
  beforeEach(() => {
    fetchAny.resetMocks();
  });
  it('retrieveAccessToken', async (): Promise<void> => {
    // GIVEN
    const serviceAuth = new ServiceAccountAuth();
    const mockRespondedAt = '1/8xbJqaOZXSUZbHLl5EOtu1pxz3fmmetKx9W8CV4t79M';
    fetchAny.mockResponseOnce(
      JSON.stringify({
        access_token: mockRespondedAt,
        token_type: 'Bearer',
        expires_in: 3600,
      })
    );
    const mockJwt = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.any';

    // WHEN
    const tokenObj = await serviceAuth.retrieveAccessToken(mockJwt);

    // THEN
    expect(tokenObj.access_token).toBe(mockRespondedAt);

    expect(fetch).toHaveBeenCalledWith('https://oauth2.googleapis.com/token', {
      body: `grant_type=urn%3Aietf%3Aparams%3Aoauth%3Agrant-type%3Ajwt-bearer&assertion=${mockJwt}`,
      cache: 'no-cache',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      method: 'POST',
    });
  });
});
