import { StackdriverLogger } from '../src/stackdriver-logger';
import { JwtGeneration } from '../src/jwt-gen';
import { ServiceAccountAuth, Token } from '../src/service-account-auth';

jest.mock('../src/jwt-gen');
jest.mock('../src/service-account-auth');

const mockPk =
  '-----BEGIN RSA PRIVATE KEY-----\n' +
  'MIICXgIBAAKBgQDXKX2/PDS9KgCUZXJJVpgZ2btHNjB4MY+SU9fT5xujHxe+ET/u\n' +
  'E0rQS2xRRbAI2ntFJI4Iw/i/DfrAGlkBPoCq/OwbjcLyajNErVkTYmXAQh84WjpC\n' +
  'x8lSf8wBMlut2KSTU4Sd5NqnweiG/+Gk/EAFRTOTCsJeQK1sDJ+frB/nvQIDAQAB\n' +
  'AoGBAIRjtkjl/blrT8H/jcvMaXh2fYJ6uGYscJ/j0QhEyr+bERvVN1YBLuZtki+L\n' +
  'xp4b+P95V5/THYpJSsHLNHrMf5QVNPCghZYXKt/tR3zaWf8qgfp67xNs7zea2TTL\n' +
  '2cM1ibfUdL4uw7gPkGrVAsMjQd28Ldl8gAv0ssCm5Sq1j7whAkEA9mByEEkJRyRh\n' +
  'dNAOr7aBNO6PvjRT28h/l5Y+sWZBK6A4zs2FkG2UtHdL5GIoAhkAdZXh/C1iGc25\n' +
  'z+yfB0sIVQJBAN+Q7QwN0zEogLsBW69fI38FzO+IeTp9sZ80UilpEsLjRQclbS+W\n' +
  'OdotAH1fyIKxtncB5A7VIGJHH1mAzt4d6ckCQQCJwA7GMC+xyRN8Hj+32pCPKbrQ\n' +
  'm60vOwCrnmNbCFLzs1dPuPUudTYpWHHkNUUtNxFcrPp25mDtEGsNZ7zadQcpAkEA\n' +
  'h5lZjcQc4mYUnlhWieoEl5inEmoOTYHLaCrN4rwOWxKJ3am/retKL6C3+VNNRKgR\n' +
  'aeFFuBFg0PsecqUD1AkEsQJAXTYZQkxXQI72SJEuEd0ldu9C6jOIC7UMywHi7LbV\n' +
  '7vljm0pM1rwaBDSrY+Qw96a+nUotSPVpIjf4q4Dr010myw==\n' +
  '-----END RSA PRIVATE KEY-----';

describe('StackdriverLogger', (): void => {
  const fetchAny = fetch as any;
  beforeEach(() => {
    jest.resetAllMocks();
    const mockCreateJwt = jest
      .fn()
      .mockResolvedValue('eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.any');

    const mockRetrieveAccessToken = jest.fn().mockResolvedValue({
      access_token: 'atmock',
      expires_at_seconds: (Date.now() + 3600 * 1000) / 1000,
    });

    JwtGeneration.prototype.createJwt = mockCreateJwt;
    ServiceAccountAuth.prototype.retrieveAccessToken = mockRetrieveAccessToken;
    fetchAny.resetMocks();
  });
  it('log', async (): Promise<void> => {
    // GIVEN
    const logger = new StackdriverLogger('test@any.de', mockPk, 'test-project');

    const logValue = [
      {
        jsonPayload: {
          key: 'value1',
        },
      },
      {
        jsonPayload: {
          key: 'value2',
        },
      },
    ];

    // WHEN
    await logger.log(logValue);

    // THEN
    expect(fetch).toHaveBeenCalledWith(
      'https://logging.googleapis.com/v2/entries:write',
      expect.anything()
    );
  });
  it('log request token only once', async (): Promise<void> => {
    // GIVEN
    const logger = new StackdriverLogger('test@any.de', mockPk, 'test-project');

    const logValue = [
      {
        jsonPayload: {
          key: 'value1',
        },
      },
      {
        jsonPayload: {
          key: 'value2',
        },
      },
    ];

    // WHEN
    await logger.log(logValue);
    await logger.log(logValue);

    // THEN
    expect(JwtGeneration.prototype.createJwt).toBeCalledTimes(1);
    expect(ServiceAccountAuth.prototype.retrieveAccessToken).toBeCalledTimes(1);
  });
});
